/* global cegedim */
'use strict';

cegedim.flow.initSequence(false, [
    '00_COVER',
    '01_COMPARISON_OF_CHEMICAL_AND_BIOLOGICAL_DRUGS',
    '02_COMPARISON',
    '03_UNIQUE_PRODUCTION_PROCESS_OF_LMWHS',
    '04_DIFFERENTIATION_BY_QUALITY',
    '05_DIFFERENTIATION_BY_QUALITY',
    '06_BIOSIMILARS'
  ],
  false
);

cegedim.flow.initSequence('06_BIOSIMILARS', [
    '07_INJECTION',
    '08_USAGE'
],
false
);
