/* global cegedim*/
$(function() {
    $(window).load(function() {
      var $videos = $('.videos');
  
      // play
      $('.js-play').on('click', function (evt) {
        evt.stopImmediatePropagation();
  
        // stop another
  
        // play this
        var $video = $(this).siblings('video');
        setTimeout(function () {
          $video.get(0).play();
          $('.js-play').addClass('none');
          $('.videos').addClass('up');
          $('.text').addClass('nones');
          
        }, 600);
  
        $videos.attr('data-state', $video.data('state'));
      });
  
      // no play
      $('.video-overlay').on('click', function () {
        $('.js-play').not(this).siblings('video').each(function () {
          this.pause();
        });
        $videos.removeAttr('data-state');
      });
    });
  });
  