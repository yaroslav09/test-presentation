$(function() {
    $(window).load(function() {
  
      var tabs = $('#tabs');
      init();
      tabs.find('li').on('click', function(event) {
          event.stopImmediatePropagation();
          changeSlide($(event.target));
      });
  
    });
  
    function checkTabs() {
        var tabs = $('#tabs'),
            active = tabs.find('.active').attr('data-id'),
            tab2  = tabs.children('li[data-id="pane2"]'),
            pane2 = $('#pane2');
  
            if(active === 'pane1') {
                pane2.addClass('l').removeClass('r');
                tab2.removeClass('s');
            }else if(active === 'pane3') {
                pane2.addClass('r').removeClass('l');
                tab2.addClass('s');
                $('.slide-05').addClass('done3'); 
            }else if(active === 'pane2'){
          pane2.removeClass('r l');
                tab2.removeClass('s');  			
                $('.slide-05').addClass('done2');
            }
    }
  
    function changeSlide(slide) {
        var id = slide.attr('data-id');
      var delay;
        slide.addClass('active').siblings().removeClass('active');
        $('#'+id).addClass('active').siblings().removeClass('active');
      checkTabs();
      if($('.done2').length && $('.done3').length && $('.bottom-header[data-header='+id+']').is('.load')) {
        $('.bottom-header[data-header='+id+']')
            .stop(true)
            .fadeIn(300)
            .siblings('.bottom-header')       
            .stop(true)
            .fadeOut(300);
      }else {
        if (id === 'pane2') {
          delay = 4000;
        }else {
          delay = 5000;
        }
        $('.bottom-header[data-header='+id+']').siblings('.bottom-header').stop(true).fadeOut(300);
        
        setTimeout(function(){
          $('.bottom-header[data-header='+id+']').addClass('load').stop(true).fadeIn(300);
        }, delay);
      }  	    
    }
  
    function init() {
        $('#tabs li:first, #pane1').addClass('active');  	
        setTimeout(function(){
            $('#pane1, #pane2, #pane3').css('transition', 'left 0.8s cubic-bezier(0.645, 0.045, 0.355, 1) 0s');
        
        }, 1000);
      setTimeout(function(){
        $('.bottom-header[data-header="pane1"]').stop(true).fadeIn(300).addClass('load');
      },6000);
     
        checkTabs();
  
    }
  });